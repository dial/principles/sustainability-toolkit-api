# Sustainability Toolkit API Specification

The [Sustainability Toolkit API specification](../src/swagger.json) has been defined via the [OpenAPI Specification v3.0](https://swagger.io/specification/).

You can review it by starting the server and visiting `/docs`.
