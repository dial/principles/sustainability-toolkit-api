# Business Model Sustainability Toolkit API

This project powers the API behind the [Sustainability Toolkit](https://gitlab.com/dial/principles/sustainability-toolkit).

It is a simple REST API built using [ExpressJS](https://expressjs.com/).

You can learn more about the API itself by viewing our [API documentation](meta/API.md) or by starting an instance of the server and viewing the live documentation located at `/docs`.

## Local setup

Developing this project requires [yarn](https://yarnpkg.com/) to manage packages. Running the project will additionally require [PostgreSQL](https://www.postgresql.org) to store user data.

### Node version

The currently-targeted **production** version of Node is noted in `package.json` in the `"engines"` section.

The currently-targeted **development** version of Node is located in `.node-version`.  We recommend [nvm](https://github.com/nvm-sh/nvm) to manage your version of Node.

```bash
ln -s .node-version .nvmrc
nvm use
```

### Dependencies

To install dependencies:

```bash
yarn install
```

You will need to install and setup PostgreSQL separately.

### Configuration

To configure the project with environment-specific options, copy the `.env.example` files to `.env` and populate its values. Or, if you have another preferred way of managing environment variables, you can just make sure the environment variables defined in `.env` are exported.

```bash
cp .env.example .env
edit .env
```

⚠️ Never commit your populated environment variables to the repository!

### Starting the server

To run the development server:

```bash
yarn start
```

The API should be visible at [http://localhost:3001](http://localhost:3001).

If you're in a development environment you can use the following command to run a copy of the code that restarts whenever you change a file:

```bash
yarn dev
```

## Running tests

We use Jest for automated tests; code contributions should come with relevant tests and tests should be pass before opening a Merge Request.

```
yarn test
```

## License

See [LICENSE](./LICENSE) for the terms governing this software.
