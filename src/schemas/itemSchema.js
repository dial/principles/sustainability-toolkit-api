import Joi from 'joi'

export const itemSchema = Joi.object({
	userId: Joi.string()
		.required(),
	key: Joi.string()
		.required(),
	value: Joi.string()
		.required(),
})
