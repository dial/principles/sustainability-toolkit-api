import Joi from 'joi'

export const authTokenSchema = Joi.object({
	userId: Joi.string()
		.required(),
	username: Joi.string()
		.required(),
	token: Joi.string()
		.required(),
	expiresAt: Joi.date()
		.required(),
})
