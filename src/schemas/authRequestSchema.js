import Joi from 'joi'

export const authRequestSchema = Joi.object({
	nonce: Joi.string()
		.required(),
	callbackUrl: Joi.string()
		.required(),
	expiresAt: Joi.date()
		.required(),
})
