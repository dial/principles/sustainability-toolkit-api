import { AUTH_TOKEN_TIMEOUT } from '../constants'

export function generateNewAuthTokenExpirationIsoTimestamp() {
	const expiration = new Date(new Date().getTime() + AUTH_TOKEN_TIMEOUT)

	return expiration.toISOString()
}
