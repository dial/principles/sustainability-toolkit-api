// eslint-disable-next-line max-len
// NOTE: This file has been deprecated. Legacy code used to authenticate using Discourse as SSO provider

import crypto from 'crypto'
import { URL } from 'url'
import { db } from '../database'
import {
	authRequestSchema,
	authTokenSchema,
} from '../schemas'
import { AUTH_REQUEST_TIMEOUT } from '../constants'
import { generateNewAuthTokenExpirationIsoTimestamp } from '../utils'

function generateNonce() {
	return crypto.randomBytes(64).toString('base64')
}

function generateAuthToken() {
	return crypto.randomBytes(128).toString('base64')
}

function generateDiscourseSsoPayload(nonce, returnSsoUrl) {
	// Encoding the nonce itself is NOT a step according to Discourse documentation, but it
	// is necessary, as Discourse fails to URI-encode the nonce they send back, which means
	// the nonce is URI-decoded when we try to parse their payload later in the auth flow.
	const urlEncodedNonce = encodeURIComponent(nonce)
	const rawPayload = `nonce=${urlEncodedNonce}&return_sso_url=${returnSsoUrl}`
	const base64Payload = Buffer.from(rawPayload).toString('base64')

	return base64Payload
}

function generateHexSignatureForDiscourseSSO(message) {
	return crypto
		.createHmac('sha256', process.env.DISCOURSE_SSO_SECRET)
		.update(message)
		.digest('hex')
}

function isAllowedCallbackHost(host) {
	const allowedHosts = process.env.ALLOWED_CALLBACK_HOSTS.split(',')

	return allowedHosts.includes(host)
}

function getAuthRequestExpirationIsoTimestamp() {
	const expiration = new Date(new Date().getTime() + AUTH_REQUEST_TIMEOUT)

	return expiration.toISOString()
}

function decodeCallbackSso(sso) {
	const decryptedSso = Buffer.from(sso, 'base64').toString()
	const parsedSso = new URLSearchParams(decryptedSso)

	return Object.fromEntries(parsedSso)
}

/**
 * This implements the first half of the SSO procedure outlined at:
 * https://meta.discourse.org/t/using-discourse-as-an-identity-provider-sso-discourseconnect/32974
 *
 * The main steps:
 * Step 1: Generate and save a nonce
 * Step 2: Generate a Discourse SSO payload
 * Step 3: Generate a signature for the payload
 * Step 4: Redirect the user to the discourse auth request URL
 */
const auth = async (req, res) => {
	const nonce = generateNonce()
	const callbackHost = req.get('host')
	if (!isAllowedCallbackHost(callbackHost)) {
		return res
			.status(400)
			.send({ message: 'Could not create authorization request: callback host is not authorized.' })
	}

	if (!('callbackPath' in req.query)) {
		return res
			.status(400)
			.send({ message: 'Could not create authorization request: callbackPath must be defined.' })
	}

	const callbackUrl = new URL(
		req.query.callbackPath,
		`${req.protocol}://${callbackHost}`,
	)

	const authRequestValues = {
		callbackUrl: callbackUrl.href,
		nonce,
		expiresAt: getAuthRequestExpirationIsoTimestamp(),
	}
	const { error } = authRequestSchema.validate(authRequestValues)
	if (error !== undefined) {
		return res
			.status(400)
			.send({
				message: 'Could not create authorization request',
				validationError: error,
			})
	}

	const results = await db.sql(
		'authRequests.create',
		authRequestValues,
	)
	if (results.rows.length === 0) {
		return res
			.status(500)
			.send({
				message: 'Could not create authorization request',
			})
	}

	const base64Payload = generateDiscourseSsoPayload(
		nonce,
		process.env.API_AUTH_CALLBACK_URL,
	)
	const uriEncodedPayload = encodeURIComponent(base64Payload)
	const signature = generateHexSignatureForDiscourseSSO(base64Payload)
	const discourseUrl = new URL(
		`/session/sso_provider?sso=${uriEncodedPayload}&sig=${signature}`,
		process.env.DISCOURSE_SSO_DOMAIN,
	)

	return res.redirect(303, discourseUrl.href)
}

/**
 * This implements the second half of the SSO procedure outlined at:
 * https://meta.discourse.org/t/using-discourse-as-an-identity-provider-sso-discourseconnect/32974
 *
 * The main steps:
 * Step 1: Compute the HMAC-SHA256 signature of the sso using our secret
 * Step 2: Compare that signature with the sig value provided to us
 * Step 3: Base64 decode the SSO
 * Step 4: Ensure the `nonce` value in the SSO matches our stored nonce
 * Step 5: Generate and save a token for the user
 * Step 6: Redirect to the redirect associated with the nonce, including the token
 */
const authCallback = async (req, res) => {
	if (!('sso' in req.query)) {
		return res
			.status(400)
			.send({ message: 'Could not complete auth: sso must be defined.' })
	}

	if (!('sig' in req.query)) {
		return res
			.status(400)
			.send({ message: 'Could not complete auth: sig must be defined.' })
	}

	const {
		sso,
		sig,
	} = req.query
	const sigCheck = generateHexSignatureForDiscourseSSO(sso)
	if (sig !== sigCheck) {
		return res
			.status(400)
			.send({ message: 'Could not complete auth: incorrect signature.' })
	}

	const decodedSso = decodeCallbackSso(sso)
	const fetchResults = await db.sql(
		'authRequests.fetchUnexpiredByNonce',
		{ nonce: decodedSso.nonce },
	)
	if (fetchResults.rows.length === 0) {
		return res
			.status(400)
			.send({ message: 'Invalid auth request, no matching nonce found.' })
	}

	const authRequest = fetchResults.rows[0]
	if (decodedSso.nonce !== authRequest.nonce) {
		return res
			.status(400)
			.send({ message: 'Invalid auth request, SSO nonce did not match auth request nonce.' })
	}

	const authTokenValues = {
		userId: decodedSso.external_id,
		username: decodedSso.username,
		token: generateAuthToken(),
		expiresAt: generateNewAuthTokenExpirationIsoTimestamp(),
	}
	const { error } = authTokenSchema.validate(authTokenValues)
	if (error !== undefined) {
		return res
			.status(400)
			.send({
				message: 'Invalid auth token parameters.',
				validationError: error,
			})
	}

	const createResults = await db.sql(
		'authTokens.create',
		authTokenValues,
	)
	if (createResults.rows.length === 0) {
		return res
			.status(500)
			.send({
				message: 'Could not create auth token.',
			})
	}

	const authToken = createResults.rows[0]
	const uriEncodedToken = encodeURIComponent(authToken.token)
	const uriEncodedUsername = encodeURIComponent(authToken.username)
	const authCallbackUrl = new URL(`${authRequest.callbackUrl}?token=${uriEncodedToken}&username=${uriEncodedUsername}`)

	return res.redirect(303, authCallbackUrl)
}

const authController = {
	auth,
	authCallback,
}

export default authController
