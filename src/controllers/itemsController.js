import { db } from '../database'
import { itemSchema } from '../schemas'

const listItems = async (req, res) => {
	if (!req.oidc || !req.oidc.user) {
		return res
			.status(403)
			.send({ message: 'You are not properly authenticated.' })
	}

	const results = await db.sql(
		'items.listByUserId',
		{ userId: req.oidc.user.sub.split('|')[1] },
	)

	return res.send(results.rows)
}

const getItem = async (req, res) => {
	if (!req.oidc || !req.oidc.user) {
		return res
			.status(403)
			.send({ message: 'You are not properly authenticated.' })
	}

	const results = await db.sql(
		'items.fetch',
		{
			userId: req.oidc.user.sub.split('|')[1],
			key: req.params.key,
		},
	)
	if (results.rows.length > 0) {
		return res.send(results.rows[0])
	}

	return res
		.status(404)
		.send({ message: 'Not Found' })
}

const saveItem = async (req, res) => {
	if (!req.oidc || !req.oidc.user) {
		return res
			.status(403)
			.send({ message: 'You are not properly authenticated.' })
	}

	const itemValues = {
		userId: req.oidc.user.sub.split('|')[1],
		key: req.params.key,
		value: JSON.stringify(req.body.value),
	}
	const { error } = itemSchema.validate(itemValues)
	if (error !== undefined) {
		return res
			.status(400)
			.send({
				message: 'Could not create Item',
				validationError: error,
			})
	}

	const results = await db.sql(
		'items.createOrUpdate',
		itemValues,
	)
	if (results.rows.length > 0) {
		return res.send(results[0])
	}

	return res
		.status(400)
		.send({ message: 'Could not create Item' })
}

const itemsController = {
	listItems,
	getItem,
	saveItem,
}

export default itemsController
