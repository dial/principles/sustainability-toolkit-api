/*
 * This dictates how long an auth token will stay valid between sessions
 * The token expiration will refresh (by this amount) every time it is used
 */
export const AUTH_TOKEN_TIMEOUT = 2592000000 // 30 days (in ms)
