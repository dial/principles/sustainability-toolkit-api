/*
 * This dictates how long an auth request will stay valid before resolving
 * This is NOT how long the resulting token will last, but rather the duration
 * of the window the user has to actually log in after initiating the auth
 * process.
 */
export const AUTH_REQUEST_TIMEOUT = 3600000 // 1 hour (in ms)
