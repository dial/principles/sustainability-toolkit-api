INSERT
INTO auth_tokens (
	user_id,
	username,
	token,
	expires_at
)
VALUES (
	:userId,
	:username,
	:token,
	:expiresAt
)
RETURNING
	id,
	user_id as userId,
	username,
	token,
	expires_at AS expiresAt,
	created_at AS createdAt
