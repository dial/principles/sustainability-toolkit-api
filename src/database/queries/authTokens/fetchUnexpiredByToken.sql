SELECT
	auth_tokens.id as "id",
	auth_tokens.user_id as "userId",
	auth_tokens.username as "username",
	auth_tokens.token as "token",
	auth_tokens.expires_at as "expiresAt",
	auth_tokens.created_at as "createdAt"
FROM auth_tokens
WHERE auth_tokens.token = :token
AND auth_tokens.expires_at > NOW()
