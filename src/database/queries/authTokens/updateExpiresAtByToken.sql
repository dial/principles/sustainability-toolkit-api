UPDATE auth_tokens
SET expires_at = :expiresAt
WHERE token = :token
