INSERT
INTO items (
	user_id,
	key,
	value
)
VALUES (
	:userId,
	:key,
	:value
)
ON CONFLICT (user_id, key) DO UPDATE
  SET value = EXCLUDED.value
RETURNING *
