SELECT
	items.user_id as "userId",
	items.key as key,
	items.value as value
FROM items
WHERE items.user_id = :userId
