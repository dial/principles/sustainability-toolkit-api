SELECT
	items.user_id as "userId",
	items.key as key,
	items.value as value
FROM items
WHERE items.key = :key
AND items.user_id = :userId
