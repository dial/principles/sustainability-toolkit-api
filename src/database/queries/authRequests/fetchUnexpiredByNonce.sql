SELECT
	auth_requests.id as "id",
	auth_requests.nonce as "nonce",
	auth_requests.callback_url as "callbackUrl",
	auth_requests.expires_at as "expiresAt",
	auth_requests.created_at as "createdAt"
FROM auth_requests
WHERE auth_requests.nonce = :nonce
AND auth_requests.expires_at > NOW()
