INSERT
INTO auth_requests (
	nonce,
	callback_url,
	expires_at
)
VALUES (
	:nonce,
	:callbackUrl,
	:expiresAt
)
RETURNING
	id,
	nonce,
	callback_url AS callbackUrl,
	expires_at AS expiresAt,
	created_at AS createdAt
