import { db } from '../database'
import { generateNewAuthTokenExpirationIsoTimestamp } from '../utils'

function isBearerTokenAuthorizationHeader(authorizationHeader) {
	return authorizationHeader.startsWith('Bearer ')
}

function extractBearerToken(authorizationHeader) {
	if (!isBearerTokenAuthorizationHeader(authorizationHeader)) {
		return ''
	}

	return authorizationHeader.slice(7) // Remove "Bearer "
}

async function refreshAuthToken(authToken) {
	await db.sql(
		'authTokens.updateExpiresAtByToken',
		{
			token: authToken.token,
			expiresAt: generateNewAuthTokenExpirationIsoTimestamp(),
		},
	)
}

async function tokenAuth(req, res, next) {
	const authorizationHeader = req.headers.authorization
	if (!authorizationHeader) {
		return res
			.status(403)
			.send({ message: 'No Authorization header provided.' })
	}

	if (!isBearerTokenAuthorizationHeader(authorizationHeader)) {
		return res
			.status(403)
			.send({ message: 'No Bearer Token provided.' })
	}

	const bearerToken = extractBearerToken(authorizationHeader)

	const fetchResults = await db.sql(
		'authTokens.fetchUnexpiredByToken',
		{ token: bearerToken },
	)
	if (fetchResults.rows.length === 0) {
		return res
			.status(403)
			.send({ message: 'Invalid Bearer Token.' })
	}

	const authToken = fetchResults.rows[0]
	req.userId = authToken.userId
	await refreshAuthToken(authToken)

	return next()
}

export default tokenAuth
