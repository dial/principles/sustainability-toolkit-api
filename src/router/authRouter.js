import express from 'express'
import Auth0 from 'express-openid-connect'

const { requiresAuth } = Auth0

function buildCallbackUrl(req, callbackPath) {
	const callbackHost = req.get('host')
	if (!process.env.ALLOWED_CALLBACK_HOSTS.split(',').includes(callbackHost)) {
		return null
	}

	const protocol = process.env.API_BASE_URL.split(':')[0]
	const callbackUrl = new URL(
		callbackPath,
		`${protocol}://${callbackHost}`,
	)

	return callbackUrl
}

const authRouter = express.Router()

authRouter.get('/signup', (req, res) => {
	res.oidc.login({
		returnTo: `/auth/auth?callbackPath=${req.query.callbackPath}`,
		authorizationParams: { screen_hint: 'signup' },
	})
})

authRouter.get('/login', (req, res) => {
	res.oidc.login({
		returnTo: `/auth/auth?callbackPath=${req.query.callbackPath}`,
	})
})

authRouter.get('/auth', requiresAuth(), (req, res) => {
	const callbackUrl = buildCallbackUrl(req, req.query.callbackPath)
	if (!callbackUrl) {
		return res
			.status(400)
			.send({ message: 'Could not create authorization request: callback host is not authorized.' })
	}

	if (!('callbackPath' in req.query)) {
		return res
			.status(400)
			.send({ message: 'Could not create authorization request: callbackPath must be defined.' })
	}

	return res.redirect(`${callbackUrl}?id=${req.oidc.user.sub.split('|')[1]}&username=${req.oidc.user.nickname}`)
})

authRouter.get('/deauth', requiresAuth(), (req, res) => {
	const callbackUrl = buildCallbackUrl(req, '/account')
	res.oidc.logout({ returnTo: callbackUrl.toString() })
})

export default authRouter
