import express from 'express'
import Auth0 from 'express-openid-connect'
import authRouter from './authRouter'
import itemsRouter from './itemsRouter'
import docsRouter from './docsRouter'

const { auth } = Auth0

const router = express.Router()

router.use('/docs', docsRouter)
router.post('/callback', (req, res, next) => {
	next()
})

router.get('/', (req, res) => {
	res.redirect('/api/docs')
})

router.use(auth({
	auth0Logout: true,
	idpLogout: true,
	authRequired: false,
	baseURL: process.env.API_BASE_URL,
	clientID: process.env.SSO_CLIENT_ID,
	issuerBaseURL: process.env.SSO_DOMAIN,
	clientSecret: process.env.SSO_SECRET,
	secret: process.env.SSO_SECRET,
	authorizationParams: {
		scope: 'openid profile email',
	},
}))

router.use('/auth', authRouter)

router.use('/items', itemsRouter)

export default router
