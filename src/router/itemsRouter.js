import express from 'express'
import Auth0 from 'express-openid-connect'
import itemsController from '../controllers/itemsController'

const { requiresAuth } = Auth0
const itemsRouter = express.Router()

itemsRouter.get('/', requiresAuth(), itemsController.listItems)

itemsRouter.get('/:key', requiresAuth(), itemsController.getItem)

itemsRouter.post('/:key', requiresAuth(), itemsController.saveItem)

export default itemsRouter
