import express from 'express'
import swaggerUi from 'swagger-ui-express'
import swaggerDocument from '../swagger.json'

const docsRouter = express.Router()

docsRouter.use(
	'/',
	swaggerUi.serve,
	swaggerUi.setup(swaggerDocument),
)

export default docsRouter
